# Telegraf BTRFS exec script

This is a python script used by Telegraf to gather data about BTRFS filesystems

Thanks to Knorrie for the [`python-btrfs`](https://github.com/knorrie/python-btrfs) library .

# Requirements

This script requires python3 with the `python-btrfs` library v10 or higher.

# Installation

- Copy the script in any directory (I put it in `/usr/local/sbin/`).
- Allow Telegraf to run it as root (required to read some attributes):
    ```
    Cmnd_Alias BTRFS = /usr/local/sbin/btrfs-monitoring
    telegraf  ALL=(ALL) NOPASSWD: BTRFS
    Defaults!BTRFS !logfile, !syslog, !pam_session
    ```
    The options are only here to reduce logging.
- Configure Telegraf to call it
    ```
    [[inputs.exec]]
      commands = [
        "/usr/bin/sudo /usr/local/sbin/btrfs-monitoring"
      ]
      data_format = "influx"
    ```

# Usage with Grafana

The [`BTRFS.json`](BTRFS.json) file can be imported in Grafana to get a basic dashboard.

# Collected data

## Filesystem information

### Tags

- FS UUID

### Data

- Devices count
- Subvolumes count

## Space info

### Tags

- FS UUID
- Profile (Single, RAID1, etc.)
- Type (Data, Metadata, System)

### Data

- Total bytes
- Used bytes

## Disk usage

### Tags

- FS UUID

### Data

- Total bytes
- Allocated bytes
- Parity bytes
- Alocatable bytes

## Raw space usage

### Tags

- FS UUID
- Profile (Single, RAID1, etc.)
- Type (Data, Metadata, System)

### Data

- Allocated bytes
- Parity bytes
- Used bytes

## Space usage by type

### Tags

- FS UUID
- Type (Data, Metadata, System)

### Data

- Allocated bytes
- Parity bytes
- Used bytes

## Device information

### Tags

- FS UUID
- Device UUID
- Device ID

### Data

- Total bytes
- Allocated bytes
- Unallocated bytes
- Used bytes

## Device Stats

### Tags

- FS UUID
- Device UUID
- Device ID

### Data

- Write Errors
- Read Errors
- Flush Errors
- Generation Errors
- Corruption errors

## Device space usage

### Tags

- FS UUID
- Device UUID
- Device ID
- Profile (Single, RAID1, etc.)
- Type (Data, Metadata, System)

### Data

- Allocated bytes
- Parity bytes
